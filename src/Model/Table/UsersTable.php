<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;


class UsersTable extends Table
{
    public function initialize(array $config)
    {
         parent::initialize($config);

        $this->setTable('users');
        $this->primaryKey('id');
      
        $this->addBehavior('Timestamp');
         
        $this->hasMany('UserTokens', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('UserProfiles', [
            'foreignKey' => 'user_id',
             'joinType' => 'INNER'
        ]);

        $this->hasMany('MenuItems', [
            'foreignKey' => 'user_id'
        ]);

         
    }

/*    public function buildRules(RulesChecker $rules)
{
    $rules->add(function ($entity, $options) {
        return !empty($entity->get('field'));
    });
    return $rules;
}*/

  
}