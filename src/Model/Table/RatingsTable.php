<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class RatingsTable extends Table
{
    public function initialize(array $config)
    {
         parent::initialize($config);

        $this->setTable('ratings');
      
        $this->addBehavior('Timestamp');
         
    }
}