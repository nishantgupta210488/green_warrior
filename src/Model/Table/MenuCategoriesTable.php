<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MenuCategoriesTable extends Table
{
    public function initialize(array $config)
    {
         parent::initialize($config);

         $this->setTable('menu_categories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Timestamp');

        /*$this->belongsTo('MenuItems', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);*/
         
    }
}