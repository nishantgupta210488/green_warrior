<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;


class UsersController extends AppController
{
	
	public function initialize()
    {
   
        parent::initialize();
        $this->Auth->allow(['index', 'login', 'logout']);

    }

    //add restaurant
        public function addRestaurant(){
            //echo '<pre>'; print_r($this->request->getData()); die;
              $users = $this->Users->newEntity();
                if ($this->request->is(['post'])) {
                    /*if ($this->Users->exists(['email' => $this->request->getData('email')])) {
                        return $this->Flash->success(__(EMAIL_EXISTS));
                    }*/
                    $users = $this->Users->patchEntity($users, $this->request->getData());
                    //echo '<pre>'; print_r($users); die;
                    $users->username = $this->request->getData('firstname'). ' '. $this->request->getData('lastname');
                    if ($this->Users->save($users)) {
                        $this->Flash->success(__('Your has been saved.'));
                        return $this->redirect(['action' => 'restautantList']);
                    }
                    $this->Flash->error(__('Unable to add your data.'));
                }
                $this->set('users', $users);
        }

    public function index()
    {
    	return $this->redirect(['action' => 'login']);
    }

	public function login()
    {
    	$this->viewBuilder()->setLayout('adminlogin');
    	if ($this->request->is('post')) {
	        $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
	            return $this->redirect($this->Auth->redirectUrl());
	        }
	        $this->Flash->error('Your username or password is incorrect.');
	    }
    }
    public function dashboard(){
        //echo 'dashboard'; die;
    }
    public function logout()
    {
        $this->Flash->success(__('You are now logged out.'));
        return $this->redirect($this->Auth->logout());
    }

     public function restautantList(){
        $this->viewBuilder()->setLayout('admindefault');
        //$conditions['not']['Users.id'] = 1;
        $conditions = ['Users.id !=' => 1, 'Users.user_type !=' => 2];
            $this->paginate = [
                'limit' => 5,
                'conditions' => $conditions
            ];
        $users = $this->Users->find()->order(['Users.id' => 'DESC']);
        $usersData = $this->paginate($users);
        //echo '<pre>'; print_r($usersData); die;
        $this->set(compact('usersData'));
   }

    /**
         * @description Function to change the Status of the User i.e approved or disapproved..
         *
         * @param type $userId
         * @param type $status
         */
    public function changeStatus($userId = NULL, $status = NULL) {
        //echo 'check'; die;
        if (!$userId) {
           $this->redirect(array('controller' => 'users', 'action' => 'restautant-list'));
        }
        $userdata = $this->Users->find()->where(['Users.id' => $userId])->first();
            $userEmail = $userdata->email;
            $newData['username'] = $userdata->username;
                if ($status == 0) {
                //mail send
                    $this->Users->updateAll(array("status" => 0), array("Users.id" => $userId));
                    $this->Flash->success(__(USER_DEACTIVATED));
                }else{
                    $email = new Email();
                        $email
                        ->template('useractive')
                        ->subject('User Active')
                        ->emailFormat('both')
                        ->viewVars($newData)
                        ->to($userEmail)
                        ->from('nishant.gupta@evontech.com')
                        ->send();
                    $this->Users->updateAll(array("status" => 1), array("Users.id" => $userId));
                    $this->Flash->success(__(USER_ACTIVATED));
                }
                $this->redirect(array('controller' => 'users', 'action' => 'restautant-list'));
            }

        

}