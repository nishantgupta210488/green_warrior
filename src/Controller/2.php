<?php
public function getRestaurantList() {

        //current user Id
        $current_user = $this->currentUser['user_id'];
        if ($this->request->is('post')) {
            // extract request data
            $postData = array(); 
            $postData = $this->request->getData();
            extract($postData);

            //echo '<pre>'; print_r($postData); die;

            $userLat = isset($userLat) ? $userLat : '';
            $userLong = isset($userLong) ? $userLong : '';
            $page = isset($page) ? $page : '';
            $category_id = isset($category_id) ? $category_id : '';
            // check for user coordinates
            if (empty($userLat) || empty($userLong)) {
                return $this->_returnJson(false, 'Please provide coordinates of user\'s location.');
            }
            // if page value will be empty, set page value equals to 1
            if (empty($page)) {
                $page = 1;
            }
            // set query parameters
            $pageCount = $page;
            $radiusDistance = 5;
            $limit = 10;
            // get count of total provider records
            $totalRecords = $this->getTotalRecordsCount($radiusDistance, $userLat, $userLong);
             if ($totalRecords > 0) {
                // get total pages count based on limit per page
                $totalPages = ceil($totalRecords/$limit);
                // check whether requested pageCount value is less than total pages
                if ($pageCount <= $totalPages) {
                    $providersList = $finalArr = array();
                    $finalArr['totalPages'] = $totalPages;
                    $finalArr['currentPage'] = $pageCount;
                    // get list of all providers with inradius of 5km from users location
                    $restaurants = $this->getRestaurantData($radiusDistance, $userLat, $userLong, $pageCount, $limit, $category_id);
                    //echo '<pre>'; print_r($restaurants); die;
                    foreach ($restaurants as $restaurant) {
                        //$restaurant = $restaurant->toArray();
                        $providerDetails = array();
                        //echo '<pre>';print_r($restaurant);
                        $providerDetails['user_id'] = $restaurant['user_profile']['user_id'];
                        $providerDetails['profile_image'] = base_url . 'img/' . 'restaurant_uploads/' . $restaurant['user_profile']['profile_image'];
                        $providerDetails['restaurant_name'] = $restaurant['user_profile']['restaurant_name'];
                        $providerDetails['restaurant_tag'] = $restaurant['user_profile']['restaurant_tag'];
                        $providerDetails['distance'] = $restaurant['distance'];
                        $providerDetails['latitude'] = $restaurant['user_profile']['latitude'];
                        $providerDetails['longitude'] = $restaurant['user_profile']['longitude'];
                         foreach ($restaurant['menu_items'] as $menuData) {
                            //echo '<pre>';print_r($menuData);
                            $providerDetails['menu'][] = array($menuData['item_name'], $menuData['menu_category']['name']);
                            //$providerDetails['menu']['item_name'][] = $menuData['item_name'];
                            //$providerDetails['category_name']['name'][] = $menuData['menu_category']['name'];

                         }

                        $providersList[] = $providerDetails;
                    }//die;
                    /*foreach ($restaurants as $restaurant) {
                        $providerDetails = array();
                        //$providerDetails['id'] = $restaurant['id'];
                        $providerDetails['user_id'] = $restaurant['UserProfiles']['user_id'];
                        //$providerDetails['user_type'] = $restaurant['user_type'];
                        $providerDetails['profile_image'] = base_url . 'img/' . 'restaurant_uploads/' . $restaurant['UserProfiles']['profile_image'];
                        $providerDetails['restaurant_name'] = $restaurant['UserProfiles']['restaurant_name'];
                        $providerDetails['restaurant_tag'] = $restaurant['UserProfiles']['restaurant_tag'];
                        $providerDetails['distance'] = $restaurant['distance'];
                        $providerDetails['latitude'] = $restaurant['UserProfiles']['latitude'];
                        $providerDetails['longitude'] = $restaurant['UserProfiles']['longitude'];

                        //$providerDetails['category_id'] = $restaurant['MenuItems']['category_id'];
                        //$providerDetails['category_name'] = $restaurant['MenuCategories']['name'];


                        //$providerDetails['item_name'] = $restaurant['MenuItems']['item_name'];
                        //$providerDetails['choice'] = $restaurant['MenuItems']['choice'];
                        $providersList[] = $providerDetails;
                    }*/
                    $finalArr['restaurants'] = array_values($providersList);
                    return $this->_returnJson(true, 'Restaurant List Successfully', $finalArr); 
                }else{return $this->_returnJson(false, 'Invalid page value.');}

             }else{ return $this->_returnJson(false, 'No records found');}

        }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

    }