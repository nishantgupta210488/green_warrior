<?php
namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController{
	public function initialize() {
		parent::initialize();
		$this->Auth->allow();
		$this->loadModel('UserTokens');
		$this->loadModel('UserProfiles');
	}

	/**
	* Return api version.
	*
	* @access public
	*
	* @return json object
	*/
	public function ping() {
		echo '2'; die;
		$result = array();
		$result['status'] = true;
		$result['message'] = 'api version v1.0';
			/*$this->set([
			        'status'     => "success",
		            'message'    => 'dddddddd' ,
			        '_serialize' => ['status', 'message']
		]);*/
		return $this->_returnJson(true, 'ping', $result);
	}

	/**
     *  Login Method
     *
     * @access public
     * @return json object
     */
	public function login() {
		if (!$this->request->is('post')) {
            return $this->_returnJson(false, 'Invalid Request.');
        } else {
        	// extract request data
            $postData = array(); 
            $postData = $this->request->getData();
            extract($postData);
            $email = isset($email) ? $email : '';
            $password = isset($password) ? $password : '';
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            $userType = isset($userType) ? $userType : '';
			if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($password)) {
				return $this->_returnJson(false, 'Please enter password');
			}
			if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please provide device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please provide device token.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please provide user type.');
			}
			// Get user details by email
			 //$userData = $this->Users->find()->where(['email' => $email])->first();
			 $userData = $this->Users->findUser($email);
			 if (!empty($userData)) {
			  	$userData = $userData->toArray();
			  	$hasher = new DefaultPasswordHasher();
			  	 // check password
			  	if ($hasher->check($password, $userData['password'])) {
			  		$user = $userDetails = array();
					$user['id'] = $userData['id'];
					$user['username'] = ucwords(strtolower($userData['username'])); 
					$user['email'] = $userData['email'];
					$user['deviceType'] = $deviceType;
					$user['deviceToken'] = $deviceToken;
					$user['userType'] = $userType;
					//create user tokens and accesstoken
					$user['accessToken'] = $this->_createOrUpdateAccessToken($userData['id'], $deviceType, $deviceToken);
					
					return $this->_returnJson(true, 'Login successfully', $user);
				
				}else{return $this->_returnJson(false, 'Incorrect password. Please try again.');}
			
			}else{return $this->_returnJson(false, 'Incorrect username. Please try again.');}
        }
	}

	//_createOrUpdateAccessToken
      /**
     * This function is used to save user details
     *
     * @access public
     * @return json object
     */
      public function register() {
        //echo '2'; die;
      	//file_put_contents('user_data.txt', print_r($this->request->getData(), true));
      	if ($this->request->is('post')) {
      		$postData = array();
            $postData = $this->request->getData();
            extract($postData);
            $username = isset($username) ? $username : '';
            $email = isset($email) ? $email : '';
            $password = isset($password) ? $password : '';
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            $userType = isset($userType) ? $userType : '';
             if (empty($username)) {
				return $this->_returnJson(false, 'Please enter username.');
			}
            if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($password)) {
				return $this->_returnJson(false, 'Please enter password');
			}
			
			if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please provide device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please provide device token.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please provide user type.');
			}
			if ($this->_checkUserExist($email, $userType)) {
				return $this->_returnJson(false, "The provided email is already registered.");
			}
			$userData = $this->Users->createUser($username, $email, $password, $userType);
			$data = [];
			$userId = $userData->id;
			$data['id'] = $userId;
			$data['username'] = $username;
			$data['email'] = $email;
			$data['user_type'] = $userType;
			$this->_createOrUpdateDeviceToken($userId, $deviceType, $deviceToken);
				return $this->_returnJson(true, 'You\'ve sign up successfully.', $data);
			}else{
				return $this->_returnJson(false, 'Invalid Request.');
			}
		}

		//_createOrUpdateDeviceToken protected function
    protected function _createOrUpdateDeviceToken($userId, $device_type, $device_token) {
		//usertoken table
		$data = $this->UserTokens->find()->where(['user_id' => $userId, 'device_type' => $device_type, 'device_token' => $device_token ])->first();
		$newData = [];
		if(empty($data)){
			$newData['UserTokens']['user_id'] = $userId;
            $newData['UserTokens']['device_type'] = $device_type;
            $newData['UserTokens']['device_token'] = $device_token;
              //create entity and patch entity
            $tokensData = $this->UserTokens->newEntity();
            $user = $this->UserTokens->patchEntity($tokensData, $newData);
            $this->UserTokens->Save($user);
          }
      }

      /**
     * Check whether email already registered.
     *
     * @access protected
     *
     * @param string $email
     * @param string $role
     * @return boolean (true if record exist)
     */
    protected function _checkUserExist($email, $userType) {
        $exists = $this->Users->exists(['email' => $email, 'user_type' => $userType]);
        return $exists;
    }

    /**
     * social Login Method
     *
     * @access public
     * @return json object
     */
     public function sociallogin(){
     	//echo '2'; die;
      	if ($this->request->is('post')) {
      		$postData = array();
            $postData = $this->request->getData();
            extract($postData);
            $email = isset($email) ? $email : '';
            $userType = isset($userType) ? $userType : '';
            $socialid = isset($socialid) ? $socialid : '';
            $provider = isset($provider) ? $provider : '';
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please enter user type.');
			}
			 if (empty($socialid)) {
				return $this->_returnJson(false, 'Please enter social id.');
			}
			 if (empty($provider)) {
				return $this->_returnJson(false, 'Please enter provider.'); // facebook/googleplus
			}
			 if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please enter device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please enter device token.');
			}

			//check email exist first empty
        	$emailData = $this->Users->find()->where(['email' => $email, 'user_type' => 2])->first();
        
       		//$emailData = $this->Users->checkEmailData($email, $userType, $socialid, $provider, $deviceType, $deviceToken);
        
        	if(empty($emailData)){
        		$newData = $profileData = [];
				$newData['email'] = $email;
				$newData['user_type'] = 2;

				//userprofile Data
				$newData['user_profile']['social_id'] = $socialid;
				$newData['user_profile']['provider'] = $provider;
				$newData['user_tokens'][] = ['device_type' => $deviceType, 'device_token' => $deviceToken];
				//$newData['user_tokens'][0]['device_type'] = "value";
				//$newData['user_tokens'][0]['device_token'] = "value";
				$userentity = $this->Users->newEntity();
				$userData = $this->Users->patchEntity($userentity, $newData, ['associated' => ['UserProfiles', 'UserTokens']]);
				$this->Users->save($userData);
				return $this->_returnJson(true, 'Login successfully');
			}else{
				//if email exist use user id
				//user profile query
				$userId = $emailData['id'];
				$checkSocialData = $this->UserProfiles->find()->where(['user_id' => $userId, 'provider' => $provider, 'social_id' => $socialid])->first();
				//$checkSocialData = $this->checkUserProfile($userId, $provider, $socialid);
				//echo '<pre>'; print_r($checkSocialData); die;
				if(!empty($checkSocialData)){
					return $this->_returnJson(true, 'Login successfully');
				}else{
					//insert data into userprofile
					$newProfileData = [];
					$newProfileData['user_id'] = $userId;
					$newProfileData['provider'] = $provider;
					$newProfileData['social_id'] = $socialid;
					$profileentity = $this->UserProfiles->newEntity();
					$profileData = $this->UserProfiles->patchEntity($profileentity, $newProfileData);
					$this->UserProfiles->save($profileData);
					return $this->_returnJson(true, 'Login successfully');
				}
			}
		}else{
			return $this->_returnJson(false, 'Invalid Request.');
		}
      }

      /**
     * forget Password Method
     *
     * @access public
     * @return json object
     */
     public function forgotPassword(){
        if ($this->request->is(['post'])) {
            $postData = array();
            $postData = $this->request->getData();
            extract($postData);
            $email = isset($email) ? $email : '';
            if (empty($email)) {
                return $this->_returnJson(false, 'Please enter email.');
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  return $this->_returnJson(false, 'Please enter a valid email.');
            }
            $emailData = $this->Users->forgotEmail($email);
            if(!empty($emailData)){
            	return $this->_returnJson(true, 'Password send to your email successfully');
            }else{
                return $this->_returnJson(false, 'No any account exists for this email');
            }
        }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

     }

}
