<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

class RestaurantsController extends AppController
{
	public $users;
	public function initialize()
    {
        parent::initialize();
       // $this->loadModel('Users');
        $users = TableRegistry::get('Users');
        
    }

   public function restautantList(){
        $this->viewBuilder()->setLayout('admindefault');
        //$conditions['not']['Users.id'] = 1;
        $conditions = ['Users.id !=' => 1, 'Users.user_type !=' => 2];
            $this->paginate = [
                'limit' => 5,
                'conditions' => $conditions
            ];
        $users = $this->Users->find()->order(['Users.id' => 'DESC']);
        $usersData = $this->paginate($users);
        //echo '<pre>'; print_r($usersData); die;
        $this->set(compact('usersData'));
   }

    /**
         * @description Function to change the Status of the User i.e approved or disapproved..
         *
         * @param type $userId
         * @param type $status
         */
    public function changeStatus($userId = NULL, $status = NULL) {
        //echo 'check'; die;
        if (!$userId) {
           $this->redirect(array('controller' => 'restaurants', 'action' => 'restautant-list'));
        }
        $userdata = $this->Users->find()->where(['Users.id' => $userId])->first();
            $userEmail = $userdata->email;
            $newData['username'] = $userdata->username;
                if ($status == 0) {
                //mail send
                    $this->Users->updateAll(array("status" => 0), array("Users.id" => $userId));
                    $this->Flash->success(__(USER_DEACTIVATED));
                }else{
                    $email = new Email();
                        $email
                        ->template('useractive')
                        ->subject('User Active')
                        ->emailFormat('both')
                        ->viewVars($newData)
                        ->to($userEmail)
                        ->from('nishant.gupta@evontech.com')
                        ->send();
                    $this->Users->updateAll(array("status" => 1), array("Users.id" => $userId));
                    $this->Flash->success(__(USER_ACTIVATED));
                }
                $this->redirect(array('controller' => 'restaurants', 'action' => 'restautant-list'));
            }

            //add restaurant
        public function addRest(){

         
            if ($this->request->is('post')) {

                
                $user = $this->users->newEntity();
                $user = $this->users->patchEntity($user, $this->request->getData());
             //echo '<pre>'; print_r($userentity); die;
             $this->users->save($user);echo "dd";exit;
            }
            
      
        }



    }




