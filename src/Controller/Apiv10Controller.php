<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

//https://www.dropbox.com/sh/6himvu6yzec27uw/AACNdLOqjEiPoGWoUe-CTRgma?dl=0
//https://docs.google.com/document/d/19axk_3-wt41A56DN_Y7dtapHSBc7jztlL63NYv9MsmE/edit

//latest Docs
//https://docs.google.com/spreadsheets/d/1xBLtgsdJBuG6OnOde2KHMUoUIew3QV719vFEW9pnPmo/edit#gid=0
class Apiv10Controller extends AppController
{

      public $autoRender = true; // Do not render any view in any action of this controller
      public $layout = null; // Set layout to null to every action of this controller
      public $autoLayout = false; // Set to false to disable automatically rendering the layout around views.
      public $current_user = array(); // Store user data after validating the access token
      public $jsonArray = array('status' => false, 'message' => 'Something went wrong'); // Set status & message.
	
	/**
     * Function used to initialize the settings for controller actions
    */
	public function initialize() {
		parent::initialize();
	}

	/**
     * check API's request and access token to authenticate users
     *
     * @access public
    */
	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		// Allow user to access all action without CakePHP Auth
		$this->Auth->allow();
		//header
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json'); 
		header("Access-Control-Allow-Methods: PUT, GET, POST, OPTIONS"); 
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization");
		//print_r($this->request->getParam('action')); die;
		// Allow user to access these function without 'Authorization' header
		$allowedFunctions = array('ping', 'login', 'register', 'sociallogin', 'forgotPassword', 'resetPassword', 'getCategoryList');

		// If a method doesn't exists in a class
		if (!method_exists($this, $this->request->getParam('action'))) {
			header('HTTP/1.0 404 Not Found');
			exit(json_encode(array('status' => false, 'message' => 'The requested api endpoint doesn\'t exist.')));
		}
		// Process all requests
		if (!in_array($this->request->getParam('action'), $allowedFunctions)) {
		// Fetch all HTTP request headers from the current request.
			$requestHeaders = apache_request_headers();

		//file_put_contents('data1.txt', print_r($requestHeaders, true));
			if (((isset($requestHeaders['Authorization']) && !empty($requestHeaders['Authorization'])) ||
				(isset($requestHeaders['authorization']) && !empty($requestHeaders['authorization'])))) {
				if (isset($requestHeaders["Authorization"]) && !empty($requestHeaders["Authorization"])) {
					$authorizationHeader = $requestHeaders['Authorization'];
				} else if (isset($requestHeaders["authorization"]) && !empty($requestHeaders["authorization"])){
					$authorizationHeader = $requestHeaders['authorization'];
				}

				if ($this->_authenticate($authorizationHeader) === FALSE) {
					header('HTTP/1.0 401 Unauthorized');
					exit(json_encode(array('status' => false, 'message' => 'No valid access token provided.')));
				}
				// check here token time expires or not ...
			} else {
				header('HTTP/1.0 401 Unauthorized');
				exit(json_encode(array('status' => false, 'message' => 'No authorization header sent.')));
			}
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			array_walk_recursive($this->request->data, array($this, '_trimElementOfArray'));
		}
	}

	// Remove whitespaces from start and end of a string from element
    protected function _trimElementOfArray(&$item, &$key) {
        $item = trim($item);
    }

    /**
     * Output given data in JSON format.
     *
     * @access protected
     *
     * @param bool $status true|false
     * @param string $message
     * @param array $data The data to output in JSON format
     * @return object
     */
    protected function _returnJson($status = false, $message = null, $data = array()) {
        // Set status
        $this->jsonArray['status'] = $status ? 'success' : 'failure';
        // Set message
        $this->jsonArray['message'] = $message;
        //Set data (if any)
        if ($data) {
            // Replace all the 'null' values with blank string
            array_walk_recursive($data, array($this, '_replaceNullWithEmptyString'));
            // Remove whitespaces from start and end of a string from element
            array_walk_recursive($data, array($this, '_trimElementOfArray'));
            
            $this->jsonArray['data'] = $data;
        }
        // Set the json-encoded data to be the body
        $this->response->body(html_entity_decode(json_encode($this->jsonArray), ENT_QUOTES, 'UTF-8'));
        $this->response->statusCode(200);
        $this->response->type('application/json');
        return $this->response;
    }

    /**
     * Replace null value to blank string from all elements of associative array.
     * This function call recursively
     *
     * @access protected
     *
     * @param string|int|null $item
     * @param string $key
     * @return void
     */
    protected function _replaceNullWithEmptyString(&$item, $key) {
        if ($item === null) {
            $item = '';
        }
    }

     /**
     * Return long-lived access token.
     *
     * @access protected
     *
     * @param int $id
     * @return string $token
     */
    protected function _getAccessToken($id) {
        try {
            // Make call to "_generateAccessToken" to get long-lived access token
            $access_token = $this->_generateAccessToken($id);

            // Return the newly created access token
            return $access_token;
        }
        catch(Exception $e) {
            return $this->_returnJson(false, $e->getMessage());
        }
    }

    /**
     * Generate unique access token to access APIs
     * It uses openssl_random_pseudo_bytes & SHA1 for generating access token
     *
     * @access protected
     *
     * @param int $id
     * @return string $token
     */
    protected function _generateAccessToken($id) {
        try {
            // Generate a random token
            $token = bin2hex(openssl_random_pseudo_bytes(16)) . SHA1(($id*time()));
            return $token;
            //return $id;
        }
        catch(Exception $e) {
            return $this->_returnJson(false, $e->getMessage());
        }
    }

    /**
     * Check against the database table if the access token is valid
     *
     * @access public
     *
     * @param string $access_token
     * @return bool true|false
     */
    protected function _authenticate($access_token) {
        return $this->_validateToken($access_token);
    }

    /**
     * This function validate token send by user.
     *
     * @access protected
     *
     * @param string $access_token
     * @return bool true|false
     */
    protected function _validateToken($access_token){
        // Check if the access token exists
        $usertoken = TableRegistry::get('UserTokens');
        $data = $usertoken->find('all', ['conditions' => ['UserTokens.access_token =' => $access_token]])->first();
        if (!empty($data)) {
            $this->currentUser = $data->toArray();
            return true;
        }
        return false;
    }

	/**
	* Return api version.
	*
	* @access public
	*
	* @return json object
	*/
	/*public function ping() {
		//$user = TableRegistry::get('Users');
        //$data = $user->find('all')->first();
        //$data = $user->find('all', ['contain' => ['UserTokens', 'UserProfiles']])->toArray();
        //echo '<pre>'; print_r($data); die;
		$result = array();
		$result['status'] = true;
		$result['message'] = 'api version v1.0';
		return $this->_returnJson(true, 'ping', $result);
	}*/

    public function ping() {
        //echo '2'; die;
        $result = array();
        $result['status'] = true;
        $result['message'] = 'api version v1.0';
          $this->set([
            'status'     => $result['status'],
            'message'    => $result['message'],
            'data'       => 'data',
            '_serialize' => ['status', 'message', 'data']
         ]);
    }

	/**
     * Cafe Login Method
     *
     * @access public
     * @return json object
     */

	public function login() {
		if (!$this->request->is('post')) {
            return $this->_returnJson(false, 'Invalid Request.');
        } else {
        	// extract request data
            $postData = array(); 
            $postData = $this->request->getData();
            extract($postData);
            $email = isset($email) ? $email : '';
            $password = isset($password) ? $password : '';
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            $userType = isset($userType) ? $userType : '';
			if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($password)) {
				return $this->_returnJson(false, 'Please enter password');
			}
			if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please provide device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please provide device token.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please provide user type.');
			}
			// Get user details by email
			 $user = TableRegistry::get('Users');
			 $userData = $user->find()->where(['email' => $email])->first();
			 if (!empty($userData)) {
			  	$userData = $userData->toArray();
			  	$hasher = new DefaultPasswordHasher();
			  	 // check password
			  	if ($hasher->check($password, $userData['password'])) {
			  		$user = $userDetails = array();
					$user['id'] = $userData['id'];
					$user['username'] = ucwords(strtolower($userData['username'])); 
					$user['email'] = $userData['email'];
					$user['deviceType'] = $deviceType;
					$user['deviceToken'] = $deviceToken;
					$user['userType'] = $userType;
					//create user tokens and accesstoken
					$user['accessToken'] = $this->_createOrUpdateAccessToken($userData['id'], $deviceType, $deviceToken);
					
					return $this->_returnJson(true, 'Login successfully', $user);
				
				}else{return $this->_returnJson(false, 'Incorrect password. Please try again.');}
			
			}else{return $this->_returnJson(false, 'Incorrect username. Please try again.');}
        }
	}

	//_createOrUpdateAccessToken update access token protected function
	protected function _createOrUpdateAccessToken($userId, $device_type, $device_token) {
		//usertoken table
		$usertokens = TableRegistry::get('UserTokens');
		$data = $usertokens->find()->where(['user_id' => $userId, 'device_type' => $device_type, 'device_token' => $device_token ])->first();
		$newData = [];
		if(!empty($data)){
			$data = $data->toArray();
			$accessToken = $data['access_token'];
				if(empty($accessToken)){
					$accessToken = $this->_getAccessToken($userId);
					$usertokens->updateAll(array("access_token" => $accessToken), array("device_type" => $device_type, "device_token" => $device_token, "user_id" => $userId));
				}
			} else {
              $accessToken = $this->_getAccessToken($userId);

              $newData['UserTokens']['user_id'] = $userId;
              $newData['UserTokens']['access_token'] = $accessToken;
              $newData['UserTokens']['device_type'] = $device_type;
              $newData['UserTokens']['device_token'] = $device_token;
              //create entity and patch entity
              $tokensData = $usertokens->newEntity();
              $user = $usertokens->patchEntity($tokensData, $newData);
              $usertokens->Save($user);
          }
          return $accessToken;
      }

      /**
     * This function is used to save user details
     *
     * @access public
     * @return json object
     */
      public function register() {
        //echo $bar = ucwords(strtolower('schin joshi')); die;
      	//file_put_contents('user_data.txt', print_r($this->request->getData(), true));
      	if ($this->request->is('post')) {
      		$postData = array();
            $postData = $this->request->getData();
            extract($postData);
            $username = isset($username) ? $username : '';
            $email = isset($email) ? $email : '';
            $password = isset($password) ? $password : '';
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            $userType = isset($userType) ? $userType : '';
             if (empty($username)) {
				return $this->_returnJson(false, 'Please enter username.');
			}
            if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($password)) {
				return $this->_returnJson(false, 'Please enter password');
			}
			
			if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please provide device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please provide device token.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please provide user type.');
			}
			if ($this->_checkUserExist($email, $userType)) {
				return $this->_returnJson(false, "The provided email is already registered.");
			}
			/*if ($password != $confirmpassword) {
				return $this->_returnJson(false, "The password and confirm password is not match.");
			}*/
			
			$user = TableRegistry::get('Users');
			
			$newData = [];
			$newData['username'] = ucwords(strtolower($username));
			$newData['email'] = $email;
			$newData['password'] = $password;
			if($userType == 'customer'){
				$newData['user_type'] = 2;
			}elseif($userType == 'cafe'){
				$newData['user_type'] = 3;
			}

			$userentity = $user->newEntity();
			$userData = $user->patchEntity($userentity, $newData);
			
			if ($user->save($userData)) {
				//last insert id
					$userId = $userData->id;
					//register user data
					$userData = $user->find()->where(['id' => $userId])->first()->toArray();
					$data = [];
					$data['id'] = $userData['id'];
					$data['username'] = $userData['username'];
					$data['email'] = $userData['email'];
					if($userData['user_type'] == 2){
						$data['user_type'] = 'customer';
					}elseif ($userData['user_type'] == 3) {
						$data['user_type'] = 'cafe';
					}
					//create user tokens and accesstoken
					$this->_createOrUpdateDeviceToken($userId, $deviceType, $deviceToken);
					return $this->_returnJson(true, 'You\'ve sign up successfully.', $data);
				}
			}else{
				return $this->_returnJson(false, 'Invalid Request.');
			}
		}

      /**
     * Check whether email already registered.
     *
     * @access protected
     *
     * @param string $email
     * @param string $role
     * @return boolean (true if record exist)
     */
    protected function _checkUserExist($email, $userType) {
    	if($userType == 'customer'){
    		$Type = 2;
    	}elseif($userType == 'cafe'){
    		$Type = 3;
    	}
    	$user = TableRegistry::get('Users');
        $exists = $user->exists(['email' => $email, 'user_type' => $Type]);
        return $exists;
    }

    //_createOrUpdateDeviceToken protected function
    protected function _createOrUpdateDeviceToken($userId, $device_type, $device_token) {
		//usertoken table
		$usertokens = TableRegistry::get('UserTokens');
		$data = $usertokens->find()->where(['user_id' => $userId, 'device_type' => $device_type, 'device_token' => $device_token ])->first();
		$newData = [];
		if(empty($data)){
			$newData['UserTokens']['user_id'] = $userId;
            $newData['UserTokens']['device_type'] = $device_type;
            $newData['UserTokens']['device_token'] = $device_token;
              //create entity and patch entity
            $tokensData = $usertokens->newEntity();
            $user = $usertokens->patchEntity($tokensData, $newData);
            $usertokens->Save($user);
          }
      }

      /**
     * social Login Method
     *
     * @access public
     * @return json object
     */
     public function sociallogin(){
      	if ($this->request->is('post')) {
      		$postData = array();
            $postData = $this->request->getData();
            extract($postData);
            $username = isset($username) ? $username : '';
            $email = isset($email) ? $email : '';
            $userType = isset($userType) ? $userType : '';
            $socialid = isset($socialid) ? $socialid : '';
            $provider = isset($provider) ? $provider : '';//facebook/googlePlus
            $deviceType = isset($deviceType) ? $deviceType : '';
            $deviceToken = isset($deviceToken) ? $deviceToken : '';
            if (empty($username)) {
                return $this->_returnJson(false, 'Please enter username.');
            }
            if (empty($email)) {
				return $this->_returnJson(false, 'Please enter email.');
			}
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				return $this->_returnJson(false, 'Please enter a valid email address.');
			}
			if (empty($userType)) {
				return $this->_returnJson(false, 'Please enter user type.');
			}
			 if (empty($socialid)) {
				return $this->_returnJson(false, 'Please enter social id.');
			}
			 if (empty($provider)) {
				return $this->_returnJson(false, 'Please enter provider.');
			}
			 if (empty($deviceType)) {
				return $this->_returnJson(false, 'Please enter device type.');
			}
			if (empty($deviceToken)) {
				return $this->_returnJson(false, 'Please enter device token.');
			}

			//check email exist first empty
			$user = TableRegistry::get('Users');
        	$emailData = $user->find()->where(['email' => $email, 'user_type' => 2])->first();
            //echo '<pre>'; print_r($emailData); die;
        	
			if(empty($emailData)){
				$user = TableRegistry::get('Users');
				$newData = $profileData = [];
                $newData['username'] = $username;
				$newData['email'] = $email;
				if($userType == 'customer'){
					$newData['user_type'] = 2;
				}

				$newData['user_profile']['social_id'] = $socialid;
				$newData['user_profile']['provider'] = $provider;
				$newData['user_tokens'][] = ['device_type' => $deviceType, 'device_token' => $deviceToken];
				//$newData['user_tokens'][0]['device_type'] = "value";
				//$newData['user_tokens'][0]['device_token'] = "value";
				$userentity = $user->newEntity();
				$userData = $user->patchEntity($userentity, $newData, ['associated' => ['UserProfiles', 'UserTokens']]);
				$user->save($userData);
                $data = [];
                 $data['userId'] = $userData->id;
                 $data['username'] = $username;
                 $data['email'] = $email;
                 $data['userType'] = $userType;
                 $data['socialId'] = $socialid;
                 $data['provider'] = $provider;
                 $data['deviceType'] = $deviceType;
                 $data['deviceToken'] = $deviceToken;
                 $data['accessToken'] = $this->_createOrUpdateAccessToken($userData->id, $deviceType, $deviceToken);
				return $this->_returnJson(true, 'Login successfully', $data);
			}else{
				//if email exist use user id
				//user profile query
				$userprofile = TableRegistry::get('UserProfiles');
				$userId = $emailData['id'];
				$checkSocialData = $userprofile->find()->where(['user_id' => $userId, 'provider' => $provider, 'social_id' => $socialid])->first();
				if(!empty($checkSocialData)){
                    //get data
                 $data['userId'] = $userId;
                 $data['username'] = $username;
                 $data['email'] = $email;
                 //$data['user_type'] = $userType;
                 $data['socialId'] = $socialid;
                 $data['provider'] = $provider;
                 $data['deviceType'] = $deviceType;
                 $data['deviceToken'] = $deviceToken;
                 $data['accessToken'] = $this->_createOrUpdateAccessToken($userId, $deviceType, $deviceToken);
					return $this->_returnJson(true, 'Login successfully', $data);
				}else{
					//insert data into userprofile
					$newProfileData = [];
					$newProfileData['user_id'] = $userId;
					$newProfileData['provider'] = $provider;
					$newProfileData['social_id'] = $socialid;
					$profileentity = $userprofile->newEntity();
					$profileData = $userprofile->patchEntity($profileentity, $newProfileData);
					$userprofile->save($profileData);
                    //data
                    $data['userId'] = $userId;
                    $data['username'] = $username;
                    $data['email'] = $email;
                    $data['userType'] = $userType;
                    $data['socialId'] = $socialid;
                    $data['provider'] = $provider;
                    $data['deviceType'] = $deviceType;
                    $data['deviceToken'] = $deviceToken;
                    $data['accessToken'] = $this->_createOrUpdateAccessToken($userId, $deviceType, $deviceToken);
					return $this->_returnJson(true, 'Login successfully', $data);
				}
			}
		}else{
			return $this->_returnJson(false, 'Invalid Request.');
		}
      }

      /**
     * Cafe List Method
     *
     * @access public
     * @return json object
     */
     public function cafeprofile(){
        $current_user_id = $this->currentUser['user_id'];
        if ($this->request->is('post')) {
            $postData = array();
            $postData = $this->request->getData();
            extract($postData);

            $restName = isset($restName) ? $restName : '';
            $restTag = isset($restTag) ? $restTag : '';
            $phoneNumber = isset($phoneNumber) ? $phoneNumber : '';
            $address = isset($address) ? $address : '';
            $cafeImage = isset($cafeImage) ? $cafeImage : '';
            $weekday = isset($weekday) ? $weekday : '';
            $startTime = isset($startTime) ? $startTime : '';
            $endTime = isset($endTime) ? $endTime : '';
            if (empty($restName)) {
                return $this->_returnJson(false, 'Please enter restaurant name.');
            }
            if (empty($restTag)) {
                return $this->_returnJson(false, 'Please enter restaurant tag.');
            }
            if (empty($phoneNumber)) {
                return $this->_returnJson(false, 'Please enter phone number.');
            }
            if (empty($address)) {
                return $this->_returnJson(false, 'Please enter address.');
            }
            if (empty($weekday)) {
                return $this->_returnJson(false, 'Please enter week day.');
            }
            /*if (empty($startTime)) {
                return $this->_returnJson(false, 'Please enter start time.');
            }
            if (empty($endTime)) {
                return $this->_returnJson(false, 'Please enter end time.');
            }*/

            //echo '<pre>'; print_r($_FILES); die;
            $userprofile = TableRegistry::get('UserProfiles');
           
            $userData = $userprofile->find()->where(['user_id' => $current_user_id])->first();
            //check file
            if(!empty($_FILES['cafeImage'])){
                $fileName = $_FILES['cafeImage']['name'];
                //check formts
                $allowedFormats = array("jpeg", "jpg", "png");
                //check file size
                if ($_FILES['cafeImage']['size'] > 5000000) {
                  $data['message'] = "file size exceeded";
                  return $this->_returnJson(false, "error", $data);
              }
              //check file extension
              $pathInfo = pathinfo($fileName);
              if (!in_array($pathInfo["extension"], $allowedFormats)) {
                  $data['message'] = "extension not supported";
                  return $this->_returnJson(false, "error", $data);
              }
              //file path
              $path = WWW_ROOT . '' . 'img/restaurant_uploads/';
              //thumbnailpath
              $file_name = time() . '_' . $fileName;
              $filePath = $path . $file_name;
              //thumbnail full path
              if (!file_exists($path)) {
                  mkdir($path, 0777);
              }
              // move Upload file
              move_uploaded_file($_FILES['cafeImage']['tmp_name'], $filePath);
              //mobile resize
              if(!empty($userData['profile_image'])){
                //unlink file
                unlink($path.$userData['profile_image']);
            }
            
        }
            //check user profile data
            if(!empty($userData)){
                $userData = $userData->toArray();
                $data = $userprofile->get($userData['id']);
                $data->restaurant_name = $restName;
                $data->restaurant_tag = $restTag;
                $data->phone_number = $phoneNumber;
                $data->address = $address;
                if(!empty($_FILES['cafeImage'])){
                    $data->profile_image = $file_name;
                }
                $userprofile->save($data);
                //timings
                $newData = [];
                $restauranttimes = TableRegistry::get('RestaurantTimes');
                $restData = $restauranttimes->find()->where(['user_id' => $current_user_id])->first();
                $startsoffset = explode(' ', $startTime);
                $endoffset = explode(' ', $endTime);
                if(empty($restData)){
                    //insert
                    $newData['user_id'] = $current_user_id;
                    $newData['weekday'] = ucfirst($weekday);
                    if($weekday == ucfirst('sunday')){
                        $newData['start_time'] = '';
                        $newData['end_time'] = '';
                        $newData['start_time_offset'] = '';
                        $newData['end_time_offset'] = '';
                    }else{
                        $newData['start_time'] = $startTime;
                        $newData['end_time'] = $endTime;
                        $newData['start_time_offset'] = $startsoffset[1];
                        $newData['end_time_offset'] = $endoffset[1]; 
                    }
                   
                    $newentity = $restauranttimes->newEntity();
                    $timeData = $restauranttimes->patchEntity($newentity, $newData);
                    $restauranttimes->save($timeData);

                }else{
                    //update
                    $restdata = $restauranttimes->get($userData['id']);
                    $restdata->weekday = ucfirst($weekday);
                    if($restdata->weekday == ucfirst('sunday')){
                        $restdata->start_time = '';
                        $restdata->end_time = '';
                        $restdata->start_time_offset = '';
                        $restdata->end_time_offset = '';
                    }else{
                        $restdata->start_time = $startTime;
                        $restdata->end_time = $endTime;
                        $restdata->start_time_offset = $startsoffset[1];
                        $restdata->end_time_offset = $endoffset[1];
                    }
                    
                    $restauranttimes->save($restdata);
                }
                return $this->_returnJson(true, 'Edit Information Successfully.');
            }

        }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

     }

     /**
     * forget Password Method
     *
     * @access public
     * @return json object
     */
     public function forgotPassword(){
        if ($this->request->is(['post'])) {
            $postData = array();
            $postData = $this->request->getData();
            extract($postData);

            $email = isset($email) ? $email : '';
            if (empty($email)) {
                return $this->_returnJson(false, 'Please enter email.');
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                  return $this->_returnJson(false, 'Please enter a valid email.');
            }
            $user = TableRegistry::get('Users');
            $emailData = $user->find()->where(['Users.email' => $email])->first();
            $six_digit_random_number = mt_rand(100000, 999999);
            //$otp_expiration = date('Y-m-d H:i:s', strtotime('+24 hours'));
            if(!empty($emailData)){
                $emailData = $emailData->toArray();
                $newData = [];
            
                $newData['username']        = $emailData['username'];
                $newData['email']        = $emailData['email'];
                $newData['password']   = $six_digit_random_number;
                //$newData['otp_expiration']   = $otp_expiration;
                //updte otp users table
                $updateUser = $user->get($emailData['id']);
                $updateUser->password = $six_digit_random_number;
                //$updateUser->otp_expiration = $otp_expiration;
                if($user->save($updateUser)){
                    $email = new Email();
                    $email
                        ->template('forgotpassword')
                        ->subject('Reset password')
                        ->emailFormat('both')
                        ->viewVars($newData)
                        ->to($emailData['email'])
                        ->from('nishant.gupta@evontech.com')
                        ->send();
                    return $this->_returnJson(true, 'Password send to your email successfully');
                }
                
            }else{
                return $this->_returnJson(false, 'No any account exists for this email');
            }
        }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

     }

     /**
     * reset Password Method
     *
     * @access public
     * @return json object
     */
     public function resetPassword(){
        if ($this->request->is(['post'])) {
            $postData = array();
            $postData = $this->request->getData();
            extract($postData);

            $otp = isset($otp) ? $otp : '';
            $newpassword = isset($newpassword) ? $newpassword : '';
            $confirmpassword = isset($confirmpassword) ? $confirmpassword : '';
            if (empty($otp)) {
                return $this->_returnJson(false, 'Please enter otp.');
            }
            if (empty($password)) {
                return $this->_returnJson(false, 'Please enter new password.');
            }
            if (empty($confirmpassword)) {
                return $this->_returnJson(false, 'Please enter confirm password.');
            }
            if($password != $confirmpassword){
                return $this->_returnJson(false, 'Password does not match. Please Enter again.');
            }

            $users = TableRegistry::get('Users');
            $otpData = $users->find()->where(['Users.forgot_otp' => $otp])->first();
            
            if(!empty($otpData )){
                $otpData = $otpData->toArray();
                $user  = $users->get($otpData['id']);
                $user->password = $password;
                $user->forgot_otp = NULL;
                $user->otp_expiration = NULL;
                $current_date = date('Y-m-d');
                $expiration_date = date( 'Y-m-d', strtotime($otpData['otp_expiration']));
                if($current_date >= $expiration_date){
                    return $this->_returnJson(false, 'Your OTP has been expired or wrong, Please try again');
                }
                if($users->save($user)){
                    return $this->_returnJson(true, 'Your password has been reset successfully');
                }else{
                    return $this->_returnJson(false, 'Some thing went wrong');
                }
            }else{
                return $this->_returnJson(false, 'Invalid otp. Please enter a correct otp.');
            }
        }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

     }

      /**
     * Get list of all categories.
     *
     * @access public
     *
     * @return json object
     */
    public function getCategoryList() {
        $categories = TableRegistry::get('MenuCategories');
        $categories =  $categories->find()->toArray();
        if (!empty($categories)) {
            $categoryData = array();
            foreach ($categories as $category) {
                $temp = array();
                $temp['id'] = $category->id;
                $temp['categoryName'] = $category->name;
                $categoryData[] = $temp;
            }
            return $this->_returnJson(true, 'Category List', $categoryData);
        } else {
            return $this->_returnJson(false, 'No records found.');
        }
    }

     /**
     * This function is used to get the list of all restaurant inside radius of 5 km from user's location
     *
     * @access public
     *
     * @return json object
     */     
    public function getRestaurantList() {

        //current user Id
        $current_user = $this->currentUser['user_id'];
        if ($this->request->is('post')) {
            // extract request data
            $postData = array(); 
            $postData = $this->request->getData();
            extract($postData);

            $userLat = isset($userLat) ? $userLat : '';
            $userLong = isset($userLong) ? $userLong : '';
            $page = isset($page) ? $page : '';
            $category_id = isset($category_id) ? $category_id : '';
            // check for user coordinates
            if (empty($userLat) || empty($userLong)) {
                return $this->_returnJson(false, 'Please provide coordinates of user\'s location.');
            }
            // if page value will be empty, set page value equals to 1
            if (empty($page)) {
                $page = 1;
            }
            // set query parameters
            $pageCount = $page;
            $radiusDistance = 1;
            $limit = 10;
            // get count of total provider records
            $totalRecords = $this->getTotalRecordsCount($radiusDistance, $userLat, $userLong);
             if ($totalRecords > 0) {
                // get total pages count based on limit per page
                $totalPages = ceil($totalRecords/$limit);
                // check whether requested pageCount value is less than total pages
                if ($pageCount <= $totalPages) {
                    $providersList = $finalArr = array();
                    $finalArr['totalPages'] = $totalPages;
                    $finalArr['currentPage'] = $pageCount;
                    // get list of all providers with inradius of 5km from users location
                    $restaurants = $this->getRestaurantData($radiusDistance, $userLat, $userLong, $pageCount, $limit, $category_id);
                    //echo '<pre>'; print_r($restaurants); die;
                    foreach ($restaurants as $restaurant) {
                        $providerDetails = array();
                        $providerDetails['user_id'] = $restaurant['user_profile']['user_id'];
                        $providerDetails['restaurant_name'] = $restaurant['user_profile']['restaurant_name'];
                        $providerDetails['distance'] = $restaurant['distance'];
                        $providerDetails['latitude'] = $restaurant['user_profile']['latitude'];
                        $providerDetails['longitude'] = $restaurant['user_profile']['longitude'];
                        //menu items data
                            foreach ($restaurant['menu_items'] as $menuData) {
                            //$providerDetails['menu'][] = array($menuData['item_name'], $menuData['menu_category']['name']);
                                $providerDetails['menus'][] = array('name' => $menuData['item_name'], 'category' => $menuData['menu_category']['name']);
                            } 
                                $providersList[] = $providerDetails;
                        }
                        $finalArr['restaurants'] = array_values($providersList);
                        return $this->_returnJson(true, 'Restaurant List Successfully', $finalArr); 
                    }else{return $this->_returnJson(false, 'Invalid page value.');}
                }else{ return $this->_returnJson(false, 'No records found');}
            }else{
                return $this->_returnJson(false, 'Invalid Request.');
            }
        }

    /**
     * To get count of total records with in specified radius from user's location
     *
     * @access public
     * 
     * @param integer $radius
     * @param integer $searchCategory
     * @param string $lat
     * @param string $long
     * @return integer 
     */
    public function getTotalRecordsCount($radius, $lat, $long) {
        $distanceField = '(6371 * acos (cos ( radians(:latitude) )
                            * cos( radians( latitude ) )
                            * cos( radians( longitude )
                            - radians(:longitude) )
                            + sin ( radians(:latitude) )
                            * sin( radians( latitude ) )))';

            $usersModel = TableRegistry::get('Users');
            $Reports = $usersModel->find()
            ->select(['Users.id', 'UserProfiles.user_id', 'distance' => $distanceField])
            ->join([
                'userprofiles' => [
                'table' => 'user_profiles',
                'type' => 'INNER',
                'conditions' => ['UserProfiles.user_id = Users.id'],
                'alias' => 'UserProfiles']])
            ->where(['Users.user_type' => 3])->having(["distance < " => $radius])  
            ->order(['distance'])
            ->bind(':latitude', $lat, 'float')
            ->bind(':longitude', $long, 'float');
            //->toArray();
            //echo '<pre>';print_r($Reports); die;
            $totalCount = $Reports->count();
            return $totalCount;
        }

     /**
     * To get list of providers upto specified limit within specified radius from user's location
     *
     * @access public
     * 
     * @param integer $radius
     * @param integer $searchCategory
     * @param string $lat
     * @param string $long
     * @param integer $page
     * @param integer $limit
     * @return array
     */
     //$condition = array('Tasks.category_id' => $providerData['category_id']);
     //{"cate_id":[1,2,3]}
    public function getRestaurantData($radius, $lat, $long, $page, $limit, $category_id) {
        $providers = array();
        // formuala to calculate to get radius distance between latitudes and longitudes with provided lat,long values 
        $distanceField = '(6371 * acos (cos ( radians(:latitude) )
                            * cos( radians( latitude ) )
                            * cos( radians( longitude )
                            - radians(:longitude) )
                            + sin ( radians(:latitude) )
                            * sin( radians( latitude ) )))';

        $condition['Users.user_type'] = 3;
         $jCondition = [];
        if (!empty($category_id)) {
           $jCondition['MenuItems.category_id IN'] = $category_id;
            //$condition['MenuItems.category_id IN (1,2)'];
        }

            $usersModel = TableRegistry::get('Users');
            $providers = $usersModel->find('all')->select(['Users.id', 'Users.email', 'Users.user_type', 'UserProfiles.user_id', 'UserProfiles.latitude', 'UserProfiles.longitude', 'UserProfiles.restaurant_name', 'distance' => $distanceField])->contain(['UserProfiles', 
                /*'MenuItems' => function(\Cake\ORM\Query $q) use ($jCondition) {
                    return $q->where($jCondition)->contain(['MenuCategories']);
                }*/
            ])
            /* ->matching('MenuItems', function ($q) use($jCondition) {
                return $q->where($jCondition);
            })*/
              
            ->where($condition)->having(["distance < " => $radius]) 
            ->order(['UserProfiles.user_id' => 'DESC'])
            ->limit($limit)
            ->page($page)
            ->bind(':latitude', $lat, 'float')
            ->bind(':longitude', $long, 'float')->distinct()->toArray();
            return $providers;
        }

   /**
     * Get rest_ratings.
     *
     * @access public
     *
     * @return json object
     */
    public function restaurantRatings() {
        $current_user = $this->currentUser['user_id'];
        //echo '<pre>'; print_r($current_user); die;
         if ($this->request->is('post')) {
            // extract request data
            $postData = array(); 
            $postData = $this->request->getData();
             //echo '<pre>'; print_r($postData); die;
            extract($postData);

            $user_id = isset($user_id) ? $user_id : '';
            $restaurant_id = isset($restaurant_id) ? $restaurant_id : '';
            $rating = isset($rating) ? $rating : '';
             if (empty($user_id)) {
                return $this->_returnJson(false, 'Please enter user id.');
            }
             if (empty($restaurant_id)) {
                return $this->_returnJson(false, 'Please enter restaurant id.');
            }
            if (empty($rating)) {
                return $this->_returnJson(false, 'Please enter rating.');
            }
            $ratings = TableRegistry::get('Ratings');
            $checkRating = $ratings->find()->where(['user_id' => $current_user, 'restaurant_id' => $restaurant_id, 'rating' => $rating])->first();
            if(!empty($checkRating)){
                return $this->_returnJson(false, 'already rate.');
            }else{
            $newData = [];
            $newData['user_id'] = $user_id;
            $newData['restaurant_id'] = $restaurant_id;
            $newData['rating'] = $rating;

            $ratingentity = $ratings->newEntity();
            $ratingData = $ratings->patchEntity($ratingentity, $newData);
            if ($ratings->save($ratingData)) {return $this->_returnJson(false, 'success.');}
        }
    }else{
            return $this->_returnJson(false, 'Invalid Request.');
        }

    }

    /**
     * Get rest_ratings.
     *
     * @access public
     *
     * @return json object
     */
    public function getRatings() {
        $ratings = TableRegistry::get('Ratings');
        $query = $ratings->find();
        $ratingAverage = $query->select(['averagerating' => $query->func()->avg('rating'), 'totalcount' => $query->func()->count('user_id'), 'restaurant_id'])->group('restaurant_id')->toArray();
        //echo '<pre>'; print_r($ratingAverage); die;
        foreach ($ratingAverage as $key => $ratingValue) {
            $data = [];
            $data['total_user'] = $ratingValue->totalcount;
            $data['restaurant_id'] = $ratingValue->restaurant_id;
            $data['rating'] = $ratingValue->averagerating;
            $newData[] = $data;
        }
        return $this->_returnJson(true, 'restaurant rating.', $newData);
    }

}