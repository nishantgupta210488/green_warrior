  <?php  echo $this->Form->create('login',array('id'=>'login-form')); ?>
          <div class="form-group has-feedback">
			<label for="email">Email</label>
            <?php echo $this->Form->input('email', array('class' => 'form-control','required'=>'required','label'=>false)); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
         
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
      <?php echo $this->Form->end(); ?>