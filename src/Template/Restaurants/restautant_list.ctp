<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Cafe List</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><?= $this->Html->link('Dashboard', ['controller' => 'users', 'action' => 'dashboard'], ['escape' => false]);?></li>
          <li class="breadcrumb-item active">Cafe List</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="card card-default">
      <div class="card-header">
        <?php echo $this->Html->link('Add Cafe', ['controller' => 'restaurants', 'action' => 'addRestaurant'],['class' => 'pull-right btn btn-primary']); ?>
      </div>
      <div class="card-body table-responsive p-0">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>SNo</th>
              <th>Username</th>
              <th>Email</th>              
              <th>Type</th>
              <th>Status</th>
              <th>Created</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          <?php 
           if(!empty($usersData)){
            $perPage = $this->Paginator->param('perPage');
            $page = $this->Paginator->param('page') - 1;
            	foreach ($usersData as $key => $userval) {?>
            <tr>
               <td><?php echo ($perPage * $page) + $key + 1 ?></td>
               <td><?php echo $userval->username;?></td>
               <td><?php echo $userval->email;?></td>
               <td><?php echo $userval->user_type;?></td>
               <td>
               <?php
               		if ($userval->status == 1) {
               		echo $this->Html->link("Active", array('action' => 'change_status', $userval->id, '0'), array('class' => 'btn btn-success', 'title' => 'Click here to Inactive'));
               		} else {
               		echo $this->Html->link("Inactive", array('action' => 'change_status', $userval->id, '1'), array('class' => 'btn btn-danger', 'title' => 'Click here to Active'));
               		}
               	?>
               		
               	</td>
               <td><?php echo date('d-m-Y', strtotime($userval->created));?></td>
                <td>
                <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['action' => 'editCafe', $userval->id], ['escape' => false]); ?> | <?= $this->Html->link('<i class="fa fa-street-view" aria-hidden="true"></i>', ['action' => 'viewCafe', $userval->id], ['escape' => false]); ?> | <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',['action' => 'deleteCafe', $userval->id],['confirm' => 'Are you sure?', 'escape' => false]); ?>                
              </td>
            </tr>
            <?php
            
            }
          }else{ 
          ?>
            <tr>
              <td colspan="5" align="center">No records found</td>
            </tr>
          <?php    
          } 
          ?>
        
          </tbody>
        </table>
         <div class="paginator">
              <ul class="pagination">
                  <?= $this->Paginator->first('<< ' . __('first')) ?>
                  <?= $this->Paginator->prev('< ' . __('previous')) ?>
                  <?= $this->Paginator->numbers() ?>
                  <?= $this->Paginator->next(__('next') . ' >') ?>
                  <?= $this->Paginator->last(__('last') . ' >>') ?>
              </ul>
              <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
          </div>
        
      </div>
      
    </div>
  </div>
</section>