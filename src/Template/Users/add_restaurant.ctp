<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Add Restaurant Content</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><?php echo $this->Html->link('Dashboard', ['controller' => 'users', 'action' => 'dashboard'], ['escape' => false]);?></li>
          <li class="breadcrumb-item active">Add Restaurant Content</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="card card-default">
      <div class="card-header">
        <?php echo $this->Html->link('Back', ['controller' => 'users', 'action' => 'restautantList'],['class' => 'btn btn-primary']); ?>
      </div>
      <?php echo $this->Form->create($users) ?>      
      <div class="card-body">        
        <div class="form-group">
          <?php echo $this->Form->input("firstname", array("class" => "form-control", "placeholder" => "Enter First name", "label" => "First name:", "autofocus" => "true")); ?>
        </div>

        <div class="form-group">
          <?php echo $this->Form->input("lastname", array("class" => "form-control", "placeholder" => "Enter Last name", "label" => "Last name:")); ?>
        </div>
        <div class="form-group">
          <?php echo $this->Form->input("email", array("type" => "email", "class" => "form-control", "placeholder" => "Enter an Email", "label" => "Email:", "autocomplete" => "off")); ?>
        </div>

        <div class="form-group">
          <?php echo $this->Form->button('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
      </div>
      <?php echo $this->Form->end() ?>
      <div class="card-footer" style="border-top: 1px solid rgba(0,0,0,.125);">
        &nbsp;&nbsp;                
      </div>
    </div>
  </div>
</section>