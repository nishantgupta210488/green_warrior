<div class="card">
	<div class="card-body login-card-body">
	<p class="login-box-msg">Sign in to start your session</p>
	<?php echo $this->Form->create() ?>
		<div class="form-group has-feedback">
			<?php echo $this->Form->control('email',['class' => 'form-control', 'placeholder' => 'Email', 'label' => false]) ?>
			<span class="fa fa-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<?php echo $this->Form->control('password',['class' => 'form-control', 'placeholder' => 'Password', 'label' => false]) ?>
			<span class="fa fa-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-4">
				<?php echo $this->Form->button('Sign In', ['class' => 'btn btn-primary btn-block']) ?>			
			</div>
		</div>
	<?php echo $this->Form->end() ?>
	</div>
</div>